﻿namespace Chat_Client_APP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxClient1 = new System.Windows.Forms.GroupBox();
            this.groupBoxClient2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textMessage = new System.Windows.Forms.TextBox();
            this.listBoxMessage = new System.Windows.Forms.ListBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonSend = new System.Windows.Forms.Button();
            this.labelIP1 = new System.Windows.Forms.Label();
            this.labelPORT1 = new System.Windows.Forms.Label();
            this.labelIP2 = new System.Windows.Forms.Label();
            this.labelPORT2 = new System.Windows.Forms.Label();
            this.groupBoxClient1.SuspendLayout();
            this.groupBoxClient2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxClient1
            // 
            this.groupBoxClient1.Controls.Add(this.labelPORT1);
            this.groupBoxClient1.Controls.Add(this.labelIP1);
            this.groupBoxClient1.Controls.Add(this.textBox4);
            this.groupBoxClient1.Controls.Add(this.textBox3);
            this.groupBoxClient1.Location = new System.Drawing.Point(27, 22);
            this.groupBoxClient1.Name = "groupBoxClient1";
            this.groupBoxClient1.Size = new System.Drawing.Size(276, 142);
            this.groupBoxClient1.TabIndex = 0;
            this.groupBoxClient1.TabStop = false;
            this.groupBoxClient1.Text = "Client 1";
            // 
            // groupBoxClient2
            // 
            this.groupBoxClient2.Controls.Add(this.labelPORT2);
            this.groupBoxClient2.Controls.Add(this.labelIP2);
            this.groupBoxClient2.Controls.Add(this.textBox2);
            this.groupBoxClient2.Controls.Add(this.textBox1);
            this.groupBoxClient2.Location = new System.Drawing.Point(431, 22);
            this.groupBoxClient2.Name = "groupBoxClient2";
            this.groupBoxClient2.Size = new System.Drawing.Size(277, 142);
            this.groupBoxClient2.TabIndex = 1;
            this.groupBoxClient2.TabStop = false;
            this.groupBoxClient2.Text = "Client 2";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(98, 42);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(172, 26);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(98, 87);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(172, 26);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(88, 58);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(182, 26);
            this.textBox3.TabIndex = 1;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(88, 90);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(182, 26);
            this.textBox4.TabIndex = 2;
            // 
            // textMessage
            // 
            this.textMessage.Location = new System.Drawing.Point(27, 397);
            this.textMessage.Name = "textMessage";
            this.textMessage.Size = new System.Drawing.Size(607, 26);
            this.textMessage.TabIndex = 2;
            this.textMessage.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // listBoxMessage
            // 
            this.listBoxMessage.FormattingEnabled = true;
            this.listBoxMessage.ItemHeight = 20;
            this.listBoxMessage.Location = new System.Drawing.Point(27, 205);
            this.listBoxMessage.Name = "listBoxMessage";
            this.listBoxMessage.Size = new System.Drawing.Size(628, 144);
            this.listBoxMessage.TabIndex = 3;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(691, 232);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(78, 34);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(713, 397);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(75, 41);
            this.buttonSend.TabIndex = 5;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // labelIP1
            // 
            this.labelIP1.AutoSize = true;
            this.labelIP1.Location = new System.Drawing.Point(7, 58);
            this.labelIP1.Name = "labelIP1";
            this.labelIP1.Size = new System.Drawing.Size(24, 20);
            this.labelIP1.TabIndex = 3;
            this.labelIP1.Text = "IP";
            // 
            // labelPORT1
            // 
            this.labelPORT1.AutoSize = true;
            this.labelPORT1.Location = new System.Drawing.Point(7, 93);
            this.labelPORT1.Name = "labelPORT1";
            this.labelPORT1.Size = new System.Drawing.Size(52, 20);
            this.labelPORT1.TabIndex = 4;
            this.labelPORT1.Text = "PORT";
            // 
            // labelIP2
            // 
            this.labelIP2.AutoSize = true;
            this.labelIP2.Location = new System.Drawing.Point(31, 45);
            this.labelIP2.Name = "labelIP2";
            this.labelIP2.Size = new System.Drawing.Size(24, 20);
            this.labelIP2.TabIndex = 4;
            this.labelIP2.Text = "IP";
            // 
            // labelPORT2
            // 
            this.labelPORT2.AutoSize = true;
            this.labelPORT2.Location = new System.Drawing.Point(31, 87);
            this.labelPORT2.Name = "labelPORT2";
            this.labelPORT2.Size = new System.Drawing.Size(52, 20);
            this.labelPORT2.TabIndex = 5;
            this.labelPORT2.Text = "PORT";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.listBoxMessage);
            this.Controls.Add(this.textMessage);
            this.Controls.Add(this.groupBoxClient2);
            this.Controls.Add(this.groupBoxClient1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBoxClient1.ResumeLayout(false);
            this.groupBoxClient1.PerformLayout();
            this.groupBoxClient2.ResumeLayout(false);
            this.groupBoxClient2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxClient1;
        private System.Windows.Forms.Label labelPORT1;
        private System.Windows.Forms.Label labelIP1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBoxClient2;
        private System.Windows.Forms.Label labelPORT2;
        private System.Windows.Forms.Label labelIP2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textMessage;
        private System.Windows.Forms.ListBox listBoxMessage;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonSend;
    }
}

